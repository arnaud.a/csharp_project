﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace BTMLibrary
{
    public class FicManager
    {
        // Write everything from the list into the file localisated by the path
        public static void MajFic(string path, List<TasktoDo> TaskList)
        {
            File.WriteAllText(path, "");
            using (StreamWriter file = new StreamWriter(path))
            {
                foreach (TasktoDo Task in TaskList)
                {
                    string ID = Task.getID().ToString();
                    file.WriteLine(ID+';'+Task.getname()+';'+Task.getdesc()+';'+Task.getDeadline());
                }
            }
        }
    }
}
