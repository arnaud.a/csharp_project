﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace BTMLibrary
{
    public class ListManager
    {
        // Generate a List from file at path. Pick up each item separated by the separator
        public static List<TasktoDo> createListFromFIC(string path, char separator)
        {
            List<TasktoDo> TaskList = new List<TasktoDo>();
            string[] lines = File.ReadAllLines(path);
            foreach (string line in lines)
            {
                string[] taskTab = line.Split(separator);

                int ID;
                System.DateTime deadline;
                bool stringToIntConv = Int32.TryParse(taskTab[0], out ID);
                bool stringToDateConv = System.DateTime.TryParse(taskTab[3], out deadline);

                if ((stringToIntConv) && (stringToDateConv))
                {
                    TasktoDo task = new TasktoDo(taskTab[1], taskTab[2], ID, deadline);
                    TaskList.Add(task);
                }
            }
            return TaskList;
        }
        // Generate proper ID even if a task has been deleted
        public static int IDGenerator(List<TasktoDo> taskList)
        {
            int compteurID = 0;
            if (taskList.Count == 0)
            {
                return compteurID;
            }
            else
            {
                foreach (TasktoDo task in taskList)
                {
                    if(task.getID() != compteurID)
                    {
                        return compteurID;
                    }
                    compteurID++;
                }
            }
            return compteurID;
        }
        // Select a Task from the list
        public static TasktoDo SelectTaskFromList(int index, List<TasktoDo> TaskList)
        {
            return TaskList[index];
        }

        // UpdateIdofEach Task in the list
        
        public static List<TasktoDo> UpdateTaskID(List<TasktoDo> taskList)
        {
            int ID = 0;
            foreach(TasktoDo task in taskList)
            {
                task.setID(ID);
                ID++;
            }
            return taskList;
        }

        // Order the list choice = 0 mean ID, choice = 1 mean NAME, choice = 2 mean Deadline. The boolean allow you to chose between ascending and descending sort.

        public static List<TasktoDo> sortList (List<TasktoDo> taskList, int choice, bool order)
        {
            if (choice == 0)
            {
               if(order)
                {
                    var list_ID = (from task in taskList
                                  orderby task.getID()
                                  select task).ToList();
                    return list_ID;
                }
               else
                {
                    var list_ID = (from task in taskList
                                  orderby task.getID() descending
                                  select task).ToList();
                    return list_ID;                     
                }
            }
            if(choice == 1)
            {
                if(order)
                {
                    var list_name = (from task in taskList
                                 orderby task.getname()
                                 select task).ToList();
                    return list_name;
                }
                else
                {
                    var list_name = (from task in taskList
                                     orderby task.getname() descending
                                     select task).ToList();
                    return list_name;
                }
            }
            if (choice == 2)
            {
                if (order)
                {
                    var list_name = (from task in taskList
                                     orderby task.getDeadline()
                                     select task).ToList();
                    return list_name;
                }
                else
                {
                    var list_name = (from task in taskList
                                     orderby task.getDeadline() descending
                                     select task).ToList();
                    return list_name;
                }
            }

            return taskList;
           
        }
    }
}