﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTMLibrary
{
    public class TasktoDo
    {
        private string name;
        private string description;
        private Nullable<int> id;
        private Nullable<System.DateTime> deadline;

        public TasktoDo()
        {
            this.name = null;
            this.description = null;
            this.id = null;
            this.deadline = null;
        }

        public override string ToString()
        {
            return string.Format("Name : {0}, description : {1}, id : {2}, deadline : {3}", this.getname(), this.getdesc(), this.getID(), this.getDeadline());
        }

        public TasktoDo(string taskName, string taskdescription, int taskid, System.DateTime taskdeadline)
        {
            this.name = taskName;
            this.description = taskdescription;
            this.id = taskid;
            this.deadline = taskdeadline;
        }

        public string getname()
        {
            return this.name;
        }

        public string getdesc()
        {
            return this.description;
        }

        public int getID()
        {
            return (int)this.id;
        }
        public System.DateTime getDeadline()
        {
            return (System.DateTime)this.deadline;
        }

        public void setName(string newName)
        {
            this.name = newName;
        }

        public void setDesc(string newDesc)
        {
            this.description = newDesc;
        }

        public void setID(int ID)
        {
            this.id = ID;
        }

        public void setDeadline(System.DateTime newDeadline)
        {
            this.deadline = newDeadline;
        }
        // If you don't want to change deadline
        public void modifyTask(string newName = "placeholder", string newDescription = "placeholder")
        {
            if ((newName != "placeholder")&&(!string.IsNullOrEmpty(newName)))
            {
                setName(newName);
            }
            if ((newDescription != "placeholder")&&(!string.IsNullOrEmpty(newDescription)))
            {
                setDesc(newDescription);
            }
        }
        // Overloaded method to change Deadline
        public void modifyTask(DateTime newDeadline, string newName = "placeholder", string newDescription = "placeholder")
        {
            if ((newName != "placeholder")&&(!string.IsNullOrEmpty(newName)))
            {
                setName(newName);
            }
            if((newDescription != "placeholder")&& (!string.IsNullOrEmpty(newDescription)))
            {
                setDesc(newDescription);
            }
            setDeadline(newDeadline);

        }
    }
}
