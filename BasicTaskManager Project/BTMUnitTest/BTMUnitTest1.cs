﻿using System;
using BTMLibrary;
using System.IO;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTMUnitTest
{
    [TestClass]
    public class BTMUnitTest1
    {
        [TestMethod]
        public void TestTaskConstructor()
        {
            DateTime testDeadLine = new DateTime(2019, 4, 25);
            TasktoDo testTask = new TasktoDo("testTask", "testDesc", 0, testDeadLine);
            Assert.AreEqual("testTask", testTask.getname());
            Assert.AreEqual("testDesc", testTask.getdesc());
            Assert.AreEqual(0, testTask.getID());
            string StringDeadLine = testTask.getDeadline().ToString();
            Assert.AreEqual("25/04/2019 00:00:00", StringDeadLine);

        }

        [TestMethod]
        public void TestModifyInListwithDate()
        {
            DateTime testDeadLine = new DateTime(2019, 4, 25);
            TasktoDo testTask = new TasktoDo("testTask", "testDesc", 0, testDeadLine);
            DateTime newDeadLine = new DateTime(2000, 4, 25);
            string newName = "Wesh";
            string newDesc = "Yo";
            testTask.modifyTask(newDeadLine,newName,newDesc);
            string StringDeadLine = testTask.getDeadline().ToString();
            Assert.AreEqual("Wesh", testTask.getname());
            Assert.AreEqual("Yo" ,testTask.getdesc());
            Assert.AreEqual("25/04/2000 00:00:00", StringDeadLine);
        }
        [TestMethod]
        public void TestModifywithoutDate()
        {
            DateTime testDeadLine = new DateTime(2019, 4, 25);
            TasktoDo testTask = new TasktoDo("testTask", "testDesc", 0, testDeadLine);
            string newName = "Wesh";
            string newDesc = "Yo";
            testTask.modifyTask(newName, newDesc);
            Assert.AreEqual("Wesh", testTask.getname());
            Assert.AreEqual("Yo", testTask.getdesc());
        }
        [TestMethod]   
        public void TestCreateList()
        {
            string path = @"tasks.txt";
            if (!File.Exists(path))
            {
                File.WriteAllText(path, "");
            }
            List<TasktoDo> TestTaskList = ListManager.createListFromFIC(path, ';');
            Assert.IsInstanceOfType(TestTaskList, typeof(List<TasktoDo>));
        }

        [TestMethod]
        public void TestIDGenerator()
        {
            List<TasktoDo> TestTaskList = new List<TasktoDo>();
            Assert.AreEqual(0, ListManager.IDGenerator(TestTaskList));
            DateTime testDeadLine = new DateTime(2019, 4, 25);
            TasktoDo testTask = new TasktoDo("testTask", "testDesc", 0, testDeadLine);
            TestTaskList.Add(testTask);
            Assert.AreEqual(1, ListManager.IDGenerator(TestTaskList));
            DateTime testDeadLine2 = new DateTime(2019, 4, 25);
            TasktoDo testTask2 = new TasktoDo("testTask", "testDesc", 2, testDeadLine);
            TestTaskList.Add(testTask2);
            Assert.AreEqual(1, ListManager.IDGenerator(TestTaskList));
        }
        [TestMethod]
        public void testCreateListFromFic()
        {
            string path = @"tests.txt";
            if(!File.Exists(path))
            {
                File.WriteAllText(path, "");
            }
            File.WriteAllText(path, "");
            using (StreamWriter file = new StreamWriter(path))
            {
                file.WriteLine(string.Format("0;La première tâche;La première description;29/04/2019 15:47:02"));
            }
            List<TasktoDo> testList = ListManager.createListFromFIC(path, ';');
            Assert.AreEqual(0, testList[0].getID());
            Assert.AreEqual("La première tâche", testList[0].getname());
            Assert.AreEqual("La première description", testList[0].getdesc());
            Assert.AreEqual("29/04/2019 15:47:02", testList[0].getDeadline().ToString());
            File.WriteAllText(path, "");
        }

        [TestMethod]

        public void testMajFic()
        {
            string path = @"tests.txt";
            if(!File.Exists(path))
            {
                File.WriteAllText(path, "");
            }
            File.WriteAllText(path, "");
            List<TasktoDo> TestTaskList = new List<TasktoDo>();
            DateTime testDeadLine = new DateTime(2019, 4, 25);
            TasktoDo testTask = new TasktoDo("testTask", "testDesc", 0, testDeadLine);
            TestTaskList.Add(testTask);
            FicManager.MajFic(path, TestTaskList);
            Assert.AreEqual("0;testTask;testDesc;25/04/2019 00:00:00\r\n", File.ReadAllText(path));
            File.WriteAllText(path, "");
        }

        [TestMethod]

        public void testMajID()
        {
            List<TasktoDo> TestTaskList = new List<TasktoDo>();
            DateTime testDeadLine = new DateTime(2019, 4, 25);
            TasktoDo testTask0 = new TasktoDo("testTask", "testDesc", 0, testDeadLine);
            TasktoDo testTask1 = new TasktoDo("testTask", "testDesc", 1, testDeadLine);
            TasktoDo testTask2 = new TasktoDo("testTask", "testDesc", 2, testDeadLine);
            TestTaskList.Add(testTask0);
            TestTaskList.Add(testTask1);
            TestTaskList.Add(testTask2);
            TestTaskList.Remove(testTask1);
            ListManager.UpdateTaskID(TestTaskList);
            Assert.AreEqual(0, testTask0.getID());
            Assert.AreEqual(1, testTask2.getID());
        }
        [TestMethod]
        public void testSortList()
        {
            List<TasktoDo> TestTaskList = new List<TasktoDo>();
            DateTime testDeadLine0 = new DateTime(2017, 4, 25);
            DateTime testDeadLine1 = new DateTime(2018, 4, 25);
            DateTime testDeadLine2 = new DateTime(2019, 4, 25);
            TasktoDo testTask0 = new TasktoDo("BtestTask", "testDesc", 0, testDeadLine2);
            TasktoDo testTask1 = new TasktoDo("CtestTask", "testDesc", 1, testDeadLine0);
            TasktoDo testTask2 = new TasktoDo("AtestTask", "testDesc", 2, testDeadLine1);
            TestTaskList.Add(testTask0);
            TestTaskList.Add(testTask1);
            TestTaskList.Add(testTask2);
            TestTaskList = ListManager.sortList(TestTaskList, 0, false);
            Assert.AreEqual(2, TestTaskList[0].getID());
            Assert.AreEqual("AtestTask", TestTaskList[0].getname());       
            TestTaskList = ListManager.sortList(TestTaskList, 0, true);
            Assert.AreEqual(0, TestTaskList[0].getID());
            Assert.AreEqual("BtestTask", TestTaskList[0].getname());          
            TestTaskList = ListManager.sortList(TestTaskList, 1, false);
            Assert.AreEqual("CtestTask", TestTaskList[0].getname());
            TestTaskList = ListManager.sortList(TestTaskList, 1, true);
            Assert.AreEqual("AtestTask", TestTaskList[0].getname());
            TestTaskList = ListManager.sortList(TestTaskList, 2, false);
            Assert.AreEqual("25/04/2019 00:00:00", TestTaskList[0].getDeadline().ToString());
            Assert.AreEqual("BtestTask", TestTaskList[0].getname());
            TestTaskList = ListManager.sortList(TestTaskList, 2, true);
            Assert.AreEqual("25/04/2017 00:00:00", TestTaskList[0].getDeadline().ToString());
            Assert.AreEqual("CtestTask", TestTaskList[0].getname());
        }

        [TestMethod]

        public void testIDGenerator()
        {
            List<TasktoDo> TestTaskList = new List<TasktoDo>();
            int testID = ListManager.IDGenerator(TestTaskList);
            Assert.AreEqual(0, testID);
            DateTime testDeadLine = new DateTime(2019, 4, 25);
            TasktoDo testTask0 = new TasktoDo("testTask", "testDesc", 0, testDeadLine);
            TestTaskList.Add(testTask0);
            testID = ListManager.IDGenerator(TestTaskList);
            Assert.AreEqual(1, testID);

        }

    }
}
