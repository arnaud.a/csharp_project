﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BTMLibrary;
using System.Windows.Forms;

namespace BasicTaskManager_Project
{
    public partial class Form1 : Form
    {
        string path = @"tasks.txt";
        List<TasktoDo> TaskList;
        bool sortedListState = false;

        public Form1()
        {
            InitializeComponent();
            if (!File.Exists(path))
            {
                File.WriteAllText(path, "");
            }
            TaskList = ListManager.createListFromFIC(path, ';');
            AfficheTask(TaskList);
            UpdateCombo(TaskList);


        }

        public void AfficheTask(List<TasktoDo> taskList)
        {
            textBox4.Text = "";
            foreach (TasktoDo task in TaskList)
            {
                string ID = task.getID().ToString();
                textBox4.Text = string.Format("{0} ID : {1} \r\n Name :  {2} \r\n Description: {3} \r\n DeadLine: {4}\r\n ----- \r\n", textBox4.Text, ID, task.getname(), task.getdesc(), task.getDeadline());
            }

        }

        public void UpdateCombo(List<TasktoDo> taskList)
        {
            comboBox_id.Items.Clear();
            foreach (TasktoDo task in taskList)
            {
                comboBox_id.Items.Add(task.getID());
            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox_name.Text) | string.IsNullOrEmpty(textBox_desc.Text))
            {
                MessageBox.Show("Erreur, veuillez remplir tous les champs", "Erreur");
            }
            else
            {
                int newID = ListManager.IDGenerator(TaskList);
                TasktoDo newTask = new TasktoDo(textBox_name.Text, textBox_desc.Text, newID, dateTimePicker.Value);
                TaskList.Add(newTask);
                MessageBox.Show("La tâche a été ajouté avec succès ");
            }
            comboBox_id.SelectedIndex = -1;
            ListManager.UpdateTaskID(TaskList);
            UpdateCombo(TaskList);
            FicManager.MajFic(path, TaskList);
            AfficheTask(TaskList);
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ';')
            {
                MessageBox.Show("Le caractère ; est interdit");
                e.Handled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (comboBox_id.SelectedItem == null)
            {
                MessageBox.Show("Il faut choisir un ID", "Erreur");
                return;
            }
            int index = (int)comboBox_id.SelectedItem; ;
            TasktoDo tasktoModify = ListManager.SelectTaskFromList(index, TaskList);
            if (checkBox1.Checked)
            {
                tasktoModify.modifyTask(dateTimePicker.Value, textBox_name.Text, textBox_desc.Text);
                MessageBox.Show("La tâche a bien été modifiée");
            }
            else
            {
                if (string.IsNullOrEmpty(textBox_name.Text) & string.IsNullOrEmpty(textBox_desc.Text))
                {
                    MessageBox.Show("Il faut remplir au moins un champs pour le modifier ! ");
                }
                else
                {
                    tasktoModify.modifyTask(textBox_name.Text, textBox_desc.Text);
                    MessageBox.Show("La tâche a bien été modifiée");
                }
            }
            comboBox_id.SelectedIndex = -1;
            ListManager.UpdateTaskID(TaskList);
            UpdateCombo(TaskList);
            FicManager.MajFic(path, TaskList);
            AfficheTask(TaskList);
        }

        private void ButtonDel_Click(object sender, EventArgs e)
        {
            if (comboBox_id.SelectedItem == null)
            {
                MessageBox.Show("Il faut choisir un ID", "Erreur");
                return;
            }
            int index = (int)comboBox_id.SelectedItem; ;
            TasktoDo tasktoDel = ListManager.SelectTaskFromList(index, TaskList);
            if (TaskList.Remove(tasktoDel))
            {
                MessageBox.Show("La tâche a bien été supprimée");
            }
            comboBox_id.SelectedIndex = -1;
            ListManager.UpdateTaskID(TaskList);
            UpdateCombo(TaskList);
            FicManager.MajFic(path, TaskList);
            AfficheTask(TaskList);
        }
        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            this.path = openFileDialog1.FileName;
            TaskList = ListManager.createListFromFIC(path, ';');
            ListManager.UpdateTaskID(TaskList);
            UpdateCombo(TaskList);
            FicManager.MajFic(path, TaskList);
            AfficheTask(TaskList);

        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FicManager.MajFic(path,TaskList);
            MessageBox.Show("La liste de tache a bien été sauvegardé dans le fichier");
        }

        private void IDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(sortedListState)
            {
                TaskList = ListManager.sortList(TaskList, 0, sortedListState);
                sortedListState = false;
            }
            else
            {
                TaskList = ListManager.sortList(TaskList, 0, sortedListState);
                sortedListState = true;
            }
            AfficheTask(TaskList);
        }

        private void NameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sortedListState)
            {
                TaskList = ListManager.sortList(TaskList, 1 ,sortedListState);
                sortedListState = false;
            }
            else
            {
                TaskList = ListManager.sortList(TaskList, 1,sortedListState);
                sortedListState = true;
            }
            AfficheTask(TaskList);
        }

        private void DateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sortedListState)
            {
                TaskList = ListManager.sortList(TaskList, 2, sortedListState);
                sortedListState = false;
            }
            else
            {
                TaskList = ListManager.sortList(TaskList, 2, sortedListState);
                sortedListState = true;
            }
            AfficheTask(TaskList);
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            string message = "Voulez vous vraiment fermer l'application ?";
            string title = "Quitter";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void QuitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message = "Voulez vous vraiment fermer l'application ?";
            string title = "Quitter";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
