BasicTaskManager Project

This project allow you to manage tasks.
It reads the file tasks.txt that is in 
@csharp_project\BasicTaskManager Project\BasicTaskManager Project\bin\Debug
and generate a List of task from it. 

3 Classes are used TasktoDo, FicManager and ListManager

The form allow you to manage the list by adding a task to the list and modify or delete any task in the list.
All the event before will update the List and the file

Any modifications of the list will generate a new ID even if a task from the middle of the list is deleted

Class diagramm in the project

Professor : Samy Delahaye

https://www.delahayeyourself.info/

Author : Arnaud ANDRE